package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{
	public int max_size = 20;
	public ArrayList<FridgeItem> items = new ArrayList<FridgeItem>();
	
	public int nItemsInFridge(){
		
		return this.items.size();
	}

	public int totalSize(){
		return this.max_size;
	}

	public boolean placeIn(FridgeItem item){
		if ((this.totalSize() -  nItemsInFridge()) > 0){
			return this.items.add(item);
		}
		else {
			return false; // because no space ofc
		}
	}

	public void takeOut(FridgeItem item){
		if(this.items.contains(item)) {
			this.items.remove(item);
		}
		else{
			throw new NoSuchElementException();
		}
	}

	public void emptyFridge(){
		this.items.clear();
	}

	public List<FridgeItem> removeExpiredFood(){
		ArrayList<FridgeItem> removed_items = new ArrayList<FridgeItem>();
		for (FridgeItem item : this.items) {
			if(item.hasExpired()){
				removed_items.add(item);
			}
		}
		for (FridgeItem item : removed_items) {
			if (items.contains(item)){
				items.remove(item);
			}
		}
		return removed_items;
	}
}
